# Introduciton
This is a solution for the second laboratory work in Fundamentals of System Programming course.

## Executables

A bash script which prints list of executable files which can be executed by the current user without specifying their paths. Sorted alphabetically descending.

## Users

A bash script which prints list of users who own more processes than specified number.
