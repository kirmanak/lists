#!/bin/bash
# USAGE: ./users.bash process_limit
limit=${1:?"Limit is not specified"}
ps -e -o user | sed '1d' | sort | uniq -c | awk "\$1 > $limit { print \$2; }" | sort
