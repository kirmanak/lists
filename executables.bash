#!/bin/bash
echo $PATH | tr ':' '\n' | xargs -I'{}' find '{}' -maxdepth 1 -executable -type f \
| xargs -n 1 basename | sort -r | uniq
